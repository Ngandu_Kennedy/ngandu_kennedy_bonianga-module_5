import 'package:flutter/material.dart';

class Categories extends StatelessWidget {
  const Categories({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Colors.black,
      title: const Text(
        'P A R T N E R S',
        style: TextStyle(
            fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      centerTitle: true,
    ));
  }
}



/*
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text(
          'H O M E',
          style: TextStyle(
              fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: Builder(builder: (context) {
        return Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
              Color.fromARGB(255, 24, 3, 99),
              Color.fromARGB(255, 59, 5, 17)
            ]),
          ),
          child: ListView(children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.all(50.0),
                child: Container(
                  height: 500,
                  width: 500,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.white),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: Image.asset(
                      'lib/images/kblogo2.png',
                      matchTextDirection: true,
                      height: 250,
                      width: 300,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color: const Color.fromARGB(255, 0, 0, 0)),
                        height: 40,
                        width: 130,
                        child: TextButton.icon(
                            icon: const Text(
                              'LAUNCH',
                            ),
                            label: const Icon(Icons.start_sharp),
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        const Login()))))),
              ],
            )
          ]),
        );
      }),
    );
  }
}
*/



 Builder(builder: (context) {
        return Container(
          color: Colors.cyan,
          child: ListView(children: [
            Padding(
              padding: const EdgeInsets.all(80.0),
              child: Container(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                        height: 40,
                        width: 150,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.black),
                        child: OutlinedButton.icon(
                            icon: const Text(
                              'PARTNERS',
                            ),
                            label: const Icon(Icons.people),
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        const Home()))))),
                Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                        height: 40,
                        width: 150,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.black),
                        child: OutlinedButton.icon(
                            icon: const Text(
                              'GALLERY',
                            ),
                            label: const Icon(Icons.photo),
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        const Login()))))),
              ],
            )
          ]),
        );
      }),



      Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              width: 650,
              height: 100,
              color: Colors.blue,
              child: const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  'We trust ourself enough to always deliver '
                  'a service beyond your expetations.',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),




          
class Logos extends StatelessWidget {
  const Logos({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: const Text(
            'L O G O S',
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: Center(
            child: ConstrainedBox(
                constraints: const BoxConstraints(
                  minWidth: 500,
                ),
                child: Builder(builder: (context) {
                  return ListView(children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                        height: 200,
                        width: 200,
                        color: Colors.blue,
                        child: Image.asset('lib/images/th.jpg'),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                          height: 200,
                          width: 200,
                          color: Colors.red,
                          child: Image.asset('lib/images/geometry.jpg'),
                        )),
                    Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                          height: 200,
                          width: 200,
                          color: Colors.green,
                        )),
                    Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                          height: 200,
                          width: 200,
                          color: Colors.yellow,
                        ))
                  ]);
                }))));
  }
}

class Posters extends StatelessWidget {
  const Posters({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: const Text(
            'P O S T E R',
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: Center(child: Builder(builder: (context) {
          return ListView(children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                height: 200,
                width: 200,
                color: Colors.blue,
                child: Image.asset('lib/images/th.jpg'),
              ),
            ),
            Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  height: 200,
                  width: 200,
                  color: Colors.red,
                  child: Image.asset('lib/images/geometry.jpg'),
                )),
            Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  height: 200,
                  width: 200,
                  color: Colors.green,
                )),
            Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  height: 200,
                  width: 200,
                  color: Colors.yellow,
                ))
          ]);
        })));
  }
}


css.

body {
  margin:0;
  height:100%;
  background: #000000;
  background-image: url("lib/images/logo2black.png");
  background-size: 100% 100%;
}

.center {
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}

.contain {
  display:block;
  width:100%; height:100%;
  object-fit: contain;
}

.stretch {
  display:block;
  width:100%; height:100%;
}

.cover {
  display:block;
  width:100%; height:100%;
  object-fit: cover;
}

@media (prefers-color-scheme: dark) {
  body {
    margin:0;
    height:100%;
   /*background: #000000;*/
    
    background-image: url("lib/images/logo2black.png");
    background-size: 100% 100%;
  }
}










body {
  margin:0;
  height:100%;
  background: #000000;
  background-image: url("lib/images/poster.png");
  background-size: 100% 100%;
}

.center {
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}

.contain {
  display:block;
  width:100%; height:100%;
  object-fit: contain;
}

.stretch {
  display:block;
  width:100%; height:100%;
}

.cover {
  display:block;
  width:100%; height:100%;
  object-fit: cover;
}

@media (prefers-color-scheme: black) {
  body {
    margin:0;
    height:100%;
    background-image: url("lib/images/poster2.png");
    background-size: 15% 100%;
  }
}






<!DOCTYPE html>
<html>
<head>
  <!--
    If you are serving your web app in a path other than the root, change the
    href value below to reflect the base path you are serving from.

    The path provided below has to start and end with a slash "/" in order for
    it to work correctly.

    For more details:
    * https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base

    This is a placeholder for base href that will be replaced by the value of
    the `--base-href` argument provided to `flutter build`.
  -->
  <base href="$FLUTTER_BASE_HREF">

  <meta charset="UTF-8">
  <meta content="IE=Edge" http-equiv="X-UA-Compatible">
  <meta name="description" content="A new Flutter project.">

  <!-- iOS meta tags & icons -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="my_app">
  <link rel="apple-touch-icon" href="icons/Icon-192.png">

  <!-- Favicon -->
  <link rel="icon" type="image/png" href="favicon.png"/>

  <title>my_app</title>
  <link rel="manifest" href="manifest.json">

  <script>
    // The value below is injected by flutter build, do not touch.
    var serviceWorkerVersion = null;
  </script>
  <!-- This script adds the flutter initialization JS code -->
  <script src="flutter.js" defer></script>
  <script src="splash/splash.js"></script>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
  <link rel="stylesheet" type="text/css" href="splash/style.css">
</head>
<body>
  <picture id="splash">
    <source srcset="lib/images/poster2.png" media="(prefers-color-scheme: light)">
    <source srcset="lib/images/poster2.png" media="(prefers-color-scheme: dark)">
    <img class="center" aria-hidden="true" src="" alt=""/>
  </picture>
  <script>
    window.addEventListener('load', function(ev) {
      // Download main.dart.js
      _flutter.loader.loadEntrypoint({
        serviceWorker: {
          serviceWorkerVersion: serviceWorkerVersion,
        }
      }).then(function(engineInitializer) {
        return engineInitializer.initializeEngine();
      }).then(function(appRunner) {
        return appRunner.runApp();
      });
    });
  </script>
</body>
</html>