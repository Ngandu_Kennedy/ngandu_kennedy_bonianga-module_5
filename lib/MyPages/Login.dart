// ignore:
// ignore_for_file: file_names

import 'package:flutter/material.dart';

import '../main.dart';
import 'edit_register.dart';
import 'dashboard.dart';

void main() {
  runApp(const MyApp());
}

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);
  @override
  // ignore: library_private_types_in_public_api
  _loginState createState() => _loginState();
}

// ignore: camel_case_types
class _loginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 3, 19, 46),
          title: const Text(
            'L O G I N',
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width * 6,
            height: MediaQuery.of(context).size.height * 3,
            child: Form(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  gradient: const LinearGradient(colors: [
                    Color.fromARGB(255, 3, 69, 80),
                    Color.fromARGB(255, 54, 116, 173),
                  ]),
                ),
                child: Builder(builder: (context) {
                  return ListView(
                    padding: const EdgeInsets.all(20.0),
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          const SizedBox(
                            height: 25,
                          ),

                          const Text(
                            'W E L C O M E ',
                            style: TextStyle(
                                fontSize: 40,
                                color: Color.fromARGB(255, 255, 255, 255),
                                fontWeight: FontWeight.w600),
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          Center(
                            child: Container(
                              height: 250,
                              width: 250,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(500),
                                  color: Colors.white),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(200.0),
                                child: Image.asset(
                                  'lib/images/kblogo2.png',
                                  matchTextDirection: true,
                                  height: 250,
                                  width: 300,
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                            ),
                          ),

                          const SizedBox(
                            height: 25,
                          ),
                          const Text(
                            "USERNAME",
                            style: TextStyle(
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          //ignore: avoid_unnecessary_containers
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              width: 400,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.white,
                              ),
                              child: const Center(
                                child: TextField(
                                  decoration: InputDecoration(
                                    prefixIcon: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 12.0),
                                      child: Icon(
                                        Icons.person,
                                        color: Color.fromARGB(255, 170, 28, 18),
                                      ), // myIcon is a 48px-wide widget.
                                    ),
                                    border: InputBorder.none,
                                    hintText: 'KENNY KINGSMAN ',
                                    contentPadding:
                                        EdgeInsets.fromLTRB(40, 16, 12, 8),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const Text(
                            "EMAIL",
                            style: TextStyle(
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          //ignore: avoid_unnecessary_containers
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              width: 400,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.white,
                              ),
                              child: const Center(
                                child: TextField(
                                  decoration: InputDecoration(
                                    prefixIcon: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 12.0),
                                      child: Icon(
                                        Icons.email_rounded,
                                        color: Color.fromARGB(255, 170, 28, 18),
                                      ), // myIcon is a 48px-wide widget.
                                    ),
                                    border: InputBorder.none,
                                    hintText: 'kennykingsman2@example.com',
                                    contentPadding:
                                        EdgeInsets.fromLTRB(40, 16, 12, 8),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Text(
                            "PASSWORD",
                            style: TextStyle(fontWeight: FontWeight.w900),
                          ),

                          // ignore: avoid_unnecessary_containers
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              width: 400,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.white,
                              ),
                              child: const Center(
                                child: TextField(
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      prefixIcon: Padding(
                                        padding: EdgeInsetsDirectional.only(
                                            start: 12.0),
                                        child: Icon(
                                          Icons.lock_open_rounded,
                                          color: Colors.red,
                                        ), // myIcon is a 48px-wide widget.
                                      ),
                                      border: InputBorder.none,
                                      hintText: '**********',
                                      contentPadding:
                                          EdgeInsets.fromLTRB(20, 18, 12, 8)),
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Container(
                                      height: 40,
                                      width: 150,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          color: Colors.black),
                                      child: OutlinedButton.icon(
                                          icon: const Text(
                                            'SIGN IN',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          label: const Icon(Icons.login),
                                          onPressed: () => Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext
                                                          context) =>
                                                      const Dashboard()))))),
                              Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Container(
                                      height: 40,
                                      width: 150,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(25),
                                        color: Colors.black,
                                      ),
                                      child: OutlinedButton.icon(
                                          icon: const Text(
                                            'SIGN UP',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          label: const Icon(
                                              Icons.app_registration_rounded),
                                          onPressed: () => Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          const Register()))))),
                            ],
                          ),
                        ],
                      ),
                    ],
                  );
                }),
              ),
            ),
          ),
        ));
  }
}
